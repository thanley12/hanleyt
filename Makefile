build:
	docker compose -f docker-compose.yml -f docker-compose.dev.yml build

build-prd:
	docker compose -f docker-compose.yml -f docker-compose.prod.yml build

up:
	docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d

up-prd:
	docker compose -f docker-compose.yml -f docker-compose.prod.yml up -d

start:
	docker compose start

stop:
	docker compose stop

backup:
	docker exec ps01  bash -c 'pg_dump -U postgres -F c -f /backup/tiersheet-$(shell date +'%Y-%m-%d-%H:%M:%S').dmp tiersheet'

restore-latest:
	@echo "dropping db"
	@# "-" prefix causes command to continue even if it fails
	-@docker exec -it ps01 su - postgres -c "dropdb tiersheet"
	@echo "create new db"
	@docker exec -it ps01 su - postgres -c "createdb -O tiersheet -T template0 tiersheet"
	@echo "restoring database"
	@docker exec -it ps01 pg_restore -U postgres -d tiersheet -1 /backup/$(shell ls -1t backups/postgresql | head -1)

shell-nginx:
	docker exec -ti nx01 sh

shell-redis:
	docker exec -ti rd01 sh

shell-tiersheet:
	docker exec -ti dg01 sh

shell-db:
	docker exec -ti ps01 bash

yarn-install:
	docker exec hanleyt-frontend-1 bash -c 'yarn install'

#make run ARGS="asdf"
yarn-add:
	docker exec hanleyt-frontend-1 bash -c 'yarn add $(ARGS)'

yarn-run:
	docker exec hanleyt-frontend-1 bash -c 'yarn run $(ARGS)'

yarn-upgrade:
	docker exec hanleyt-frontend-1 bash -c 'yarn upgrade $(ARGS)'

prettier:
	docker exec hanleyt-frontend-1 bash -c 'yarn run prettier'

test-frontend:
	docker exec hanleyt-frontend-1 bash -c 'yarn run test:unit'

lint-frontend:
	docker exec hanleyt-frontend-1 bash -c 'yarn lint'

log:
	docker-compose logs

log-nginx:
	docker-compose logs nginx

log-redis:
	docker-compose logs redis

log-backend:
	docker-compose logs backend

log-db:
	docker-compose logs postgres

collectstatic:
	docker exec dg01 /bin/sh -c "python3 manage.py collectstatic --noinput"

makemigrations:
	docker exec dg01 /bin/sh -c "python3 manage.py makemigrations --noinput"

migrate:
	docker exec dg01 /bin/sh -c "python3 manage.py migrate --noinput"

import-players:
	docker exec dg01 /bin/sh -c "python3 manage.py populate_players"

import-rotoworld:
	docker exec dg01 /bin/sh -c "python3 manage.py populate_rotoworld"

import-teams:
	docker exec dg01 /bin/sh -c "python3 manage.py populate_teams"

update-byes:
	docker exec dg01 /bin/sh -c "python3 manage.py update_byes"

update-players:
	docker exec dg01 /bin/sh -c "python3 manage.py update_players"

scrape-roto:
	cd scripts/ && docker build -t rotoworld/scrape . && docker run -it -v $(shell pwd)/scripts/scrape-data:/scripts/scrape-data/ rotoworld/scrape python /scripts/rotoworld.py

#not working
scrape-teams:
	cd scripts/ && docker build -t rotoworld/scrape . && docker run -it -v $(shell pwd)/scripts/scrape-data:/scripts/scrape-data/ rotoworld/scrape python /scripts/teams.py

scrape-players:
	cd scripts/ && docker build -t rotoworld/scrape . && docker run -it -v $(shell pwd)/scripts/scrape-data:/scripts/scrape-data/ rotoworld/scrape python /scripts/fantasypros.py

create-users:
	cd scripts/users && yarn install && node users.js --createUsers

# use uid from firebase
#make create-admin ARGS="asdf"
create-admin :
	cd scripts/users && yarn install && node users.js --admin $(ARGS
