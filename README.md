Fantasy Football Tiersheet

This is a Fantasy Football tiersheet built in VueJS and Python/Django with a Postgres backend.

What is a tiersheet? It is a way to determine which players fall into different point tiers respective to their position. When you are drafting players, you can see who is available and if there are players that are still available in a higher tier.

You can also "blackout" players by double clicking on them to denote that they have been drafted.

There are also "info" links that allow you to view the player's stats or recent news in order to make a quick decision during your draft.

Each player module shows [Player Name (Team Name) Bye Week Info Link]


# Husky
Run `yarn run prepare` outside of docker container in src folder of frontend
