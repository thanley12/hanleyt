#!/bin/bash
openssl req -x509 -out dev.fantasytiersheet.com.crt -keyout dev.fantasytiersheet.com.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=dev.fantasytiersheet.com' -extensions EXT -config <( \
   printf "[dn]\nCN=dev.fantasytiersheet.com\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:dev.fantasytiersheet.com\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

#openssl req \
#-new \
#-newkey rsa:4096 \
#-days 3650 \
#-nodes \
#-x509 \
#-subj "/C=US/ST=CA/L=SF/O=dev.fantasytiersheet/CN=dev.fantasytiersheet.com" \
#-keyout dev.fantasytiersheet.com.key \
#-out dev.fantasytiersheet.com.crt

cat dev.fantasytiersheet.com.crt dev.fantasytiersheet.com.key > dev.fantasytiersheet.com.pem
#pth=/home/hanleyt/hanleyt

#docker run -it --rm \
#-v $pth/certs:/etc/letsencrypt \
#-v $pth/certs-data:/data/letsencrypt \
#-v "/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
#certbot/certbot \
#certonly -n -m thanley11@gmail.com \
#--webroot \
#--agree-tos \
#--webroot-path=/data/letsencrypt \
#--staging \
#-d fantasytiersheet.com -d www.fantasytiersheet.com
