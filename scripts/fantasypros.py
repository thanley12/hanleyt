import os
import re, mmap
from os.path import join, getsize
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import json

url = "https://www.fantasypros.com/nfl/rankings/ppr-superflex-cheatsheets.php"
csv_path = "/scripts/scrape-data/players.csv"
json_path = "/scripts/scrape-data/fantasypros.json"


def start():
    """Start web driver"""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.implicitly_wait(10)
    try:
        driver.get(url)
        content = driver.execute_script("return ecrData;")
        json_object = json.dumps(content, indent=4)
        write_file(json_object)
        driver.quit()

    except NoSuchElementException as ex:
        print(ex.msg)


# def bsoup(content):
    # soup = BeautifulSoup(content, "html.parser")
    # write_html(soup)
    # players = []
    # for tr in soup.find_all('tr'):
        # for player in tr.find_all('td',{"class":"player-cell"}):
            # name = anchor.text.strip()
            # url = prefix + anchor.get('href')
    #         players += player
    # print(players)
    # write_csv(players)


def write_file(json):
    with open(json_path, 'w') as jsonfile:
        jsonfile.write(json)


# def write_csv(roto_urls):
    # with open(csv_path, 'w') as csvfile:
        # filewriter = csv.writer(csvfile, delimiter=',',
                                # quotechar='"', quoting=csv.QUOTE_MINIMAL)

        # filewriter.writerow(['Name', 'Url'])
        # for name, url in roto_urls.items():
#             filewriter.writerow([name, url])


if __name__ == '__main__':
    print("Starting scrape script")
    # os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'backend.settings')
    # import django
    # django.setup()
    # from players.models import Rotoworld_Url
    start()

# import re, mmap
# from os.path import join, getsize
# import csv
# import pandas as pd
# from bs4 import BeautifulSoup
# import html5lib
# from urllib.request import urlopen, Request

# url="https://www.fantasypros.com/nfl/rankings/half-point-ppr-cheatsheets.php?print=true"
# csv_path = "/scripts/scrape-data/players.csv"

# def scrape(url):
    # hdr = {'User-Agent': 'Mozilla/5.0'}
    # req = Request(url,headers=hdr)
    # page = urlopen(req)
    # content = BeautifulSoup(page, "html.parser")

    # return content

# def start():
    # try:
        # soup = scrape(url)
        # # Remove abrev names
        # # for span in soup.find_all("span", {'class': 'short-name'}):
            # # span.decompose()
        # # Remove tiers
        # # for tr in soup.find_all("tr", {'class': 'tier-row'}):
            # # tr.decompose()
        # players = pd.read_html(str(soup), header=0)
        # print(players)
        # # df = players[0]
        # # # df.rename(columns = {'WSID':'Team','Overall (Team)':'Name'}, inplace = True)
        # # # Remove extra columns
        # # stripped = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # # for idx, row in stripped.iterrows():
            # # stripped.loc[idx,'Team'] = str(row['Player Name'])[-3:].strip()
            # # team = stripped.loc[idx,'Team'] = str(row['Player Name'])[-3:].strip()
            # # position = str(row['Pos'])
            # # # Remove integers from position
            # # stripped.loc[idx, 'Pos'] = re.sub(r'\d+', '', position)
            # # if 'DST' in position:
                # # stripped.loc[idx,'Team'] = re.search('\(([^)]+)', row['Player Name']).group(1)
                # # stripped.loc[idx, 'Player Name'] = str(row['Player Name'])[:-5].strip()
            # # else:
                # # stripped.loc[idx, 'Team'] = str(row['Player Name'])[-3:].strip()
                # # stripped.loc[idx, 'Player Name'] = str(row['Player Name'])[:-3].strip()
            # # if 'FA' in team:
                # # stripped.loc[idx, 'Bye'] = 0
        # # stripped.to_csv(csv_path, index=False)

    # except ValueError as err:
        # print(err.args)

# if __name__== '__main__':
    # print ("Starting scrape script")
    # start()
# import re, mmap
# from os.path import join, getsize
# import csv
# import pandas as pd
# from bs4 import BeautifulSoup
# import html5lib
# from urllib.request import urlopen, Request

# url="https://www.fantasypros.com/nfl/rankings/half-point-ppr-cheatsheets.php?print=true"
# csv_path = "/scripts/scrape-data/players.csv"

# def scrape(url):
    # hdr = {'User-Agent': 'Mozilla/5.0'}
    # req = Request(url,headers=hdr)
    # page = urlopen(req)
    # content = BeautifulSoup(page, "html.parser")

    # return content

# def start():
    # try:
        # soup = scrape(url)
        # # Remove abrev names
        # # for span in soup.find_all("span", {'class': 'short-name'}):
            # # span.decompose()
        # # Remove tiers
        # # for tr in soup.find_all("tr", {'class': 'tier-row'}):
            # # tr.decompose()
        # players = pd.read_html(str(soup), header=0)
        # print(players)
        # # df = players[0]
        # # # df.rename(columns = {'WSID':'Team','Overall (Team)':'Name'}, inplace = True)
        # # # Remove extra columns
        # # stripped = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # # for idx, row in stripped.iterrows():
            # # stripped.loc[idx,'Team'] = str(row['Player Name'])[-3:].strip()
            # # team = stripped.loc[idx,'Team'] = str(row['Player Name'])[-3:].strip()
            # # position = str(row['Pos'])
            # # # Remove integers from position
            # # stripped.loc[idx, 'Pos'] = re.sub(r'\d+', '', position)
            # # if 'DST' in position:
                # # stripped.loc[idx,'Team'] = re.search('\(([^)]+)', row['Player Name']).group(1)
                # # stripped.loc[idx, 'Player Name'] = str(row['Player Name'])[:-5].strip()
            # # else:
                # # stripped.loc[idx, 'Team'] = str(row['Player Name'])[-3:].strip()
                # # stripped.loc[idx, 'Player Name'] = str(row['Player Name'])[:-3].strip()
            # # if 'FA' in team:
                # # stripped.loc[idx, 'Bye'] = 0
        # # stripped.to_csv(csv_path, index=False)

    # except ValueError as err:
        # print(err.args)

# if __name__== '__main__':
    # print ("Starting scrape script")
    # start()
