import re, mmap
from os.path import join, getsize
import csv
from bs4 import BeautifulSoup
import html5lib
import pandas as pd
from urllib.request import urlopen, Request

url="https://fantasyfootballcalculator.com/adp/half-ppr"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/running-back"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/wide-receiver"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/quarterback"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/tight-end"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/defense"
"https://fantasyfootballcalculator.com/adp/half-ppr/12-team/kicker"

csv_path = "/scripts/scrape-data/players.csv"

def scrape(url):
    hdr = {'User-Agent': 'Mozilla/5.0'}
    req = Request(url,headers=hdr)
    page = urlopen(req)
    content = BeautifulSoup(page, "html.parser")

    return content

def start():
    try:
        soup = scrape(url)
        players = pd.read_html(str(soup), header=0)
        df = players[0]
        df.to_csv(csv_path, index=False)

    except ValueError as err:
        print(err.args)

if __name__== '__main__':
    print ("Starting scrape script")
    start()
