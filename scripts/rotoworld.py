import os
import time
import re, mmap
from os.path import join, getsize
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
import csv

url = "https://www.nbcsportsedge.com/football/nfl/depth-charts"
prefix = "https://www.nbcsportsedge.com"
roto_urls = {}
csv_path = "/scripts/scrape-data/rotoworld.csv"
html_path = "/scripts/scrape-data/rotoworld.html"


def start():
    """Start web driver"""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    try:
        driver.get(url)
        # Wait for team container to appear on page
        el = WebDriverWait(driver, timeout=3).until(lambda d: d.find_element(By.CLASS_NAME, "team-listing__container"))
        content = driver.page_source
        bsoup(content)
        driver.quit()

    except NoSuchElementException as ex:
        print(ex.msg)


def bsoup(content):
    soup = BeautifulSoup(content, "html.parser")
    for li in soup.find_all('li', {"class": "depth-chart-list-item"}):
        for anchor in li.find_all('a', href=True):
            name = anchor.text.strip()
            url = prefix + anchor.get('href')
            roto_urls[name] = url
    write_csv(roto_urls)


def write_html(html):
    with open(html_path, 'w') as htmlfile:
        htmlfile.write(html)


def write_csv(roto_urls):
    with open(csv_path, 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)

        filewriter.writerow(['Name', 'Url'])
        for name, url in roto_urls.items():
            filewriter.writerow([name, url])


if __name__ == '__main__':
    print("Starting scrape script")
    # os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'backend.settings')
    # import django
    # django.setup()
    # from players.models import Rotoworld_Url
    start()
