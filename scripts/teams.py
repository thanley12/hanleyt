import re, mmap
import pandas as pd
from os.path import join, getsize
import csv
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from urllib.parse import parse_qsl

url = "https://www.fantasypros.com/nfl/bye-weeks.php"
csv_path = "/scripts/scrape-data/teams.csv"


def scrape(url):
    hdr = {'User-Agent': 'Mozilla/5.0'}
    req = Request(url, headers=hdr)
    page = urlopen(req)
    content = BeautifulSoup(page, "html.parser")

    return content


def start():
    try:
        soup = scrape(url)
        players = pd.read_html(str(soup), header=0)
        df = players[0]
        df['Abrev'] = getAbrev(soup)
        df.to_csv(csv_path, index=False)

    except ValueError as err:
        print(err.args)


def getAbrev(soup):
    links = []
    for tr in soup.findAll("tr"):
        trs = tr.findAll("td")
        for each in trs:
            try:
                link = each.find('a')['href']
                team = parse_qsl(link)
                query = team[0][0]
                if "team" in query:
                    abrev = team[0][1]
                    if abrev == 'JAC':
                        abrev = 'JAX'
                    links.append(abrev)
            except:
                pass

    return links


if __name__ == '__main__':
    print("Starting scrape script")
    start()

