var admin = require("firebase-admin");
var fs = require("fs");

var serviceAccount = require("../../secrets/google-services.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});


users = [
  { name: "malvine", email: "malvine@example.com", password: "passpass" },
  { name: "piero", email: "piero@example.com", password: "passpass" },
  { name: "alice", email: "alice@example.com", password: "passpass" }
];

createUsers = () => {

  for (var i = 0; i < users.length; i++) {
     admin.auth().createUser({
      email: users[i]["email"],
      emailVerified: false,
      password: users[i]["password"],
    }).then(function() {
        console.log("Created new user");
      })
      .catch(function(error) {
        console.log(error.message);
      });
  }
}

const addToAdmin = (uid) => {


  admin
    .auth()
    .setCustomUserClaims(uid, { admin: true })
    .then(() => {
          console.log(`User ${uid} added as admin`);
    });
}

var myArgs = process.argv.slice(2);

switch (myArgs[0]) {
  case '--createUsers':
      return createUsers();
      break;
  case '--admin':
      const uid = myArgs[1]
      if(uid){
         return addToAdmin(uid);
      } else {
          console.log('Please provide a uid');
      }
      break;
  default:
      console.log('Sorry, that is not something I know how to do.');
}
