import firebase_admin
from rest_framework import authentication
from rest_framework import exceptions
from firebase_admin import auth
from firebase_admin import exceptions as fb_exceptions
from django.contrib.auth import get_user_model

User = get_user_model()

default_app = firebase_admin.initialize_app()


class FirebaseAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):

        auth_header = request.COOKIES.get("JWT_ACCESS_HEADER_PAYLOAD")
        signature = request.COOKIES.get("JWT_ACCESS_SIGNATURE")
        if not auth_header:
            raise exceptions.AuthenticationFailed("No auth token provided")

        decoded_token = None
        try:
            auth_header_signature = auth_header + "." + signature
            decoded_token = auth.verify_session_cookie(auth_header_signature)

        except auth.InvalidSessionCookieError:
            raise exceptions.AuthenticationFailed("Invalid auth token")
            pass
        except Exception as e:
            raise exceptions.AuthenticationFailed("Invalid auth token")
            pass

        if not auth_header_signature or not decoded_token:
            return None

        try:
            uid = decoded_token.get("uid")
        except Exception as e:
            raise fb_exceptions.FirebaseError

        user, created = User.objects.get_or_create(username=uid)

        return (user, None)
