from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Settings


class SettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "token") + (
            "is_staff",
            "is_active",
            "date_joined",
        )
        read_only_fields = ("is_staff", "is_active", "date_joined")
