from django.conf.urls import url

from .views import SettingsDetail, AuthenticateView

urlpatterns = [
    url(r"^authenticate", AuthenticateView.as_view()),
    url(r"^settings/$", SettingsDetail.as_view()),
]
