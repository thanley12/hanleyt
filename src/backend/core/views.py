from django.contrib.auth.models import User
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from firebase_admin import auth
import datetime
from .models import Settings
from .serializers import SettingsSerializer, UserSerializer
from django.conf import settings


class SettingsDetail(RetrieveAPIView):
    serializer_class = SettingsSerializer

    def get_object(self):
        queryset = Settings.objects.all().first()
        return queryset


class AuthenticateView(APIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def post(self, request):

        id_token = request.data.get("idToken")
        expires_in = datetime.timedelta(days=5)

        session_cookie = auth.create_session_cookie(id_token, expires_in=expires_in)

        header, payload, signature = session_cookie.split(".")

        response = Response(status=200)
        max_age = 365 * 24 * 60 * 60

        expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age)

        response.set_cookie(
            key="JWT_ACCESS_HEADER_PAYLOAD",
            value=header + "." + payload,
            httponly=False,
            secure=not settings.DEBUG,
            domain=settings.TIERSHEET_DOMAIN,
            expires=expires.strftime("%a, %d-%b-%Y %H:%M:%S UTC"),
            max_age=max_age,
            samesite="Lax",
        )

        response.set_cookie(
            key="JWT_ACCESS_SIGNATURE",
            value=signature,
            httponly=True,
            secure=not settings.DEBUG,
            domain=settings.TIERSHEET_DOMAIN,
            expires=expires.strftime("%a, %d-%b-%Y %H:%M:%S UTC"),
            max_age=max_age,
            samesite="Lax",
        )
        return response
