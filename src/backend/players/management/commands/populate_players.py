import csv
import os
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth import get_user_model
from players.models import Player, Tiersheet, Ranking, Team
from django.db import IntegrityError


class Command(BaseCommand):
    help = "Populates the player database"

    def handle(self, *args, **options):
        self.populate_players(start=0)
        self.populate_rankings()

    def populate_rankings(self):

        User = get_user_model()
        try:
            u = User.objects.get(id=1)
            t = Tiersheet.objects.create(user=u, title='Admin', is_default=True)
        except (User.DoesNotExist, User.MultipleObjectsReturned):
            pass

        for p in Player.objects.all():
            try:
                Ranking.objects.create(tiersheet=t, player=p, rank=p.rank)
            except IntegrityError as e:
                raise CommandError('Error "%s" does not exist' % e)

    def populate_players(self, start):
        csv_path = "/scrape-data"
        os.chdir(csv_path)
        with open("players.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            qb, rb, wr, te, dst, k = start, start, start, start, start, start

            for row in reader:
                try:

                    if row["Pos"] == 'QB':
                        new_rank = qb
                        qb += 1
                    if row["Pos"] == 'RB':
                        new_rank = rb
                        rb += 1
                    if row["Pos"] == 'WR':
                        new_rank = wr
                        wr += 1
                    if row["Pos"] == 'TE':
                        new_rank = te
                        te += 1
                    if row["Pos"] == 'DEF':
                        row["Pos"] = 'DST'
                        new_rank = dst
                        dst += 1
                    if row["Pos"] == 'PK':
                        row["Pos"] = 'K'
                        new_rank = k
                        k += 1
                    if new_rank % 5 == 0:
                        tier_num = (new_rank // 5) + 1
                        tier_name = "%s Tier %s" % (row["Pos"], tier_num)
                        Player.objects.update_or_create(
                            name=tier_name,
                            position=row["Pos"],
                            rank=new_rank,
                            url=None,
                        )
                        new_rank += 1
                    team = Team.objects.get(abrev=row["Team"])
                    Player.objects.get_or_create(
                        name=row["Name"],
                        position=row["Pos"],
                        url=None,
                        team=team,
                        rank=new_rank,
                    )

                except Team.DoesNotExist as e:
                    raise CommandError('Error team "%s" does not exist' % e)
                except IntegrityError as e:
                    raise CommandError('Error "%s" does not exist' % e)
