import csv
import os
from django.core.management.base import BaseCommand
from players.models import Player
from django.db import IntegrityError


class Command(BaseCommand):
    help = "Populates the urls for players in database"

    def handle(self, *args, **options):
        csv_path = "/scrape-data"
        os.chdir(csv_path)
        with open("rotoworld.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                try:
                    Player.objects.filter(name=str(row["Name"])).update(
                        url=str(row["Url"])
                    )
                except IntegrityError:
                    pass
                    # raise CommandError('Error "%s" does not exist' % c)

            self.stdout.write(self.style.SUCCESS("Successfully imported urls"))
