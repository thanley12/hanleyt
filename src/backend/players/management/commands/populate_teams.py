import csv
import os
from django.core.management.base import BaseCommand
from players.models import Player, Team
from django.db import IntegrityError


class Command(BaseCommand):
    help = "Populates the teams in the database"

    def handle(self, *args, **options):
        csv_path = "/scrape-data"
        os.chdir(csv_path)
        with open("teams.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            Team.objects.get_or_create(
                id=0,
                name="N/A",
                abrev="",
                bye=0
            )
            Team.objects.get_or_create(
                id=1,
                name="Free Agent",
                abrev="FA",
                bye=0
            )
            for idx, row in enumerate(reader):
                start = idx + 2
                try:
                    abrev = row["Abrev"]
                    Team.objects.get_or_create(
                        id=(start + 1),
                        name=row["Team"],
                        abrev=abrev,
                        bye=row["Bye Week"]
                    )
                except IntegrityError:
                    pass
                    # raise CommandError('Error "%s" does not exist' % c)

            self.stdout.write(self.style.SUCCESS("Successfully imported teams"))
