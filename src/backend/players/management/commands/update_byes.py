import csv
import os
import json
from django.core.management.base import BaseCommand, CommandError
from players.models import Player, Team
from django.db import IntegrityError


class Command(BaseCommand):
    help = "Updates the teams byes in the database"

    def handle(self, *args, **options):
        csv_path = "/scrape-data"
        os.chdir(csv_path)
        with open("fantasypros.json") as jsonfile:
            try:
                data = json.load(jsonfile)
                players = data['players']
                teams = {}
                for i in players:
                    if i['player_bye_week'] is not None:
                        teams[i['player_team_id']] = i['player_bye_week']
                for team, bye in teams.items():
                    if team == 'JAC':
                        team = 'JAX'
                    Team.objects.filter(abrev=str(team)).update(bye=str(bye))

            except IntegrityError:
                raise CommandError('Error "%s" does not exist' % c)

            self.stdout.write(self.style.SUCCESS("Successfully imported teams"))
