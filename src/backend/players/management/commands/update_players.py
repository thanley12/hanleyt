import os
import json
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import MultipleObjectsReturned
from players.models import Player, Tiersheet, Ranking, Team
from django.db import IntegrityError


class Command(BaseCommand):
    help = "Update the player database"

    def handle(self, *args, **options):
        self.update_players()

    def update_players(self):
        csv_path = "/scrape-data"
        os.chdir(csv_path)
        with open("fantasypros.json") as jsonfile:
            data = json.load(jsonfile)
            players = data['players']
            admin_tiersheet = Tiersheet.objects.get(is_default=True)

            try:
                for i in players:

                    name = i['player_name']
                    position = i['player_position_id']
                    team_id = i['player_team_id']
                    if team_id == 'JAC':
                        team_id = 'JAX'
                    team = Team.objects.get(abrev=team_id)
                    pos_rank = i['pos_rank']
                    rank = int("".join([ch for ch in pos_rank if ch.isdigit()]))
                    obj, created = Player.objects.update_or_create(
                        name=name,
                        defaults={'team': team, 'position': position, 'rank': rank },
                    )
                    Ranking.objects.update_or_create(
                            tiersheet=admin_tiersheet,
                            player=obj,
                            defaults={'rank': rank },
                            )

            except IntegrityError as e:
                raise CommandError('Error "%s" does not exist' % e)

            self.stdout.write(self.style.SUCCESS("Successfully updated players"))
