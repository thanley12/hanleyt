# Generated by Django 3.0.5 on 2020-07-07 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("players", "0005_auto_20200626_1927"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="player",
            options={"ordering": ["rank"], "verbose_name_plural": "players"},
        ),
        migrations.AddField(
            model_name="player",
            name="rank",
            field=models.IntegerField(default=0),
        ),
    ]
