# Generated by Django 3.0.5 on 2020-07-11 22:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("players", "0009_auto_20200711_2217"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="ranking",
            name="player_id",
        ),
        migrations.RemoveField(
            model_name="ranking",
            name="tiersheet_id",
        ),
        migrations.AddField(
            model_name="ranking",
            name="player",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="players.Player",
            ),
        ),
        migrations.AddField(
            model_name="ranking",
            name="tiersheet",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="players.Tiersheet",
            ),
        ),
    ]
