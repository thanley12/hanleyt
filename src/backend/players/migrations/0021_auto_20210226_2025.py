# Generated by Django 3.0.5 on 2021-02-26 20:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0020_auto_20210101_0136'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, default=None, max_length=128, null=True)),
                ('abrev', models.CharField(max_length=3)),
                ('bye', models.IntegerField(null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='player',
            name='bye',
        ),
        migrations.RemoveField(
            model_name='player',
            name='team',
        ),
    ]
