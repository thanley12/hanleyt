import uuid
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128, null=True, default=None,
                            blank=True)
    abrev = models.CharField(max_length=3, null=False)
    bye = models.IntegerField(null=True)


class Player(models.Model):

    POSITION_CHOICES = (
        ("QB", "Quarterback"),
        ("RB", "Runningback"),
        ("WR", "Wide Receiver"),
        ("TE", "Tight End"),
        ("DST", "Defense"),
        ("K", "Kicker"),
    )

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    position = models.CharField(max_length=3, choices=POSITION_CHOICES)
    url = models.URLField(null=True, blank=True)
    rank = models.IntegerField(default=0)
    team = models.ForeignKey(Team, on_delete=models.SET_DEFAULT, default=0)

    class Meta:
        ordering = ["rank"]
        verbose_name_plural = "players"

    def __unicode__(self):
        return self.name


class Tiersheet(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(max_length=128, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_default = models.BooleanField(default=False)


class Ranking(models.Model):
    tiersheet = models.ForeignKey(Tiersheet, on_delete=models.CASCADE,
                                  null=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE, null=True)
    rank = models.IntegerField(null=True, blank=True)
    is_starred = models.BooleanField(default=False)
