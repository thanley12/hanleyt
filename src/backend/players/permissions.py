from rest_framework import permissions
from .models import Tiersheet


class IsOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        tiersheet_id = view.kwargs.get("pk", None)
        return Tiersheet.objects.filter(user=request.user, id=tiersheet_id).exists()
