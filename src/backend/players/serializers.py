from rest_framework import serializers
from .models import Player, Tiersheet, Ranking, Team


class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = "__all__"


class PlayerUpdateSerializer(serializers.ModelSerializer):
    team = TeamSerializer(read_only=True)

    team_id = serializers.PrimaryKeyRelatedField(
        queryset=Team.objects.all(), source='team', write_only=True)

    class Meta:
        model = Player
        fields = "__all__"
        # ("id", "name", "position", "url", "team_id")


class PlayerSerializer(serializers.ModelSerializer):
    team = TeamSerializer(read_only=True)

    class Meta:
        model = Player
        fields = ("id", "name", "position", "url", "team")


class TiersheetSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()

    class Meta:
        model = Tiersheet
        user = serializers.HiddenField(default=serializers.CurrentUserDefault())

        fields = ("id", "title", "is_default", "date_created", "title")

    def get_title(self, obj):
        return "%s %s" % (obj.title, obj.date_created.strftime("%B %d, %Y"))


class RankingDetailSerializer(serializers.ModelSerializer):
    player = PlayerSerializer(read_only=True)

    class Meta:
        model = Ranking
        fields = ("id", "player", "rank", "is_starred")
