from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
    TeamList,
    PlayerList,
    TiersheetList,
    Star,
    RankingsDetail,
    RankingsDefault,
)

urlpatterns = [
    path("players/", PlayerList.as_view()),
    path("players/<int:player_id>", PlayerList.as_view()),
    path("teams/", TeamList.as_view()),
    path("tiersheets/", TiersheetList.as_view()),
    path("rankings/", RankingsDefault.as_view()),
    path("rankings/<uuid:pk>/", RankingsDetail.as_view()),
    path("rankings/<uuid:pk>/star/<int:player_id>", Star.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
