from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.views import APIView
from rest_framework import viewsets, views
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.filters import OrderingFilter
from django.contrib.auth.models import User
from django.http import Http404
from .models import Player, Tiersheet, Ranking, Team, Team
from .permissions import IsOwner
from .serializers import PlayerSerializer, TiersheetSerializer,\
                        RankingDetailSerializer, TeamSerializer, PlayerUpdateSerializer
from core.auth import FirebaseAuthentication
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

class PageNumberPaginationDataOnly(PageNumberPagination):
    page_size = 50
    def get_paginated_response(self, data):
        return Response(data)

class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = "page_size"
    max_page_size = 10000


class PlayerList(ListAPIView):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    pagination_class = LargeResultsSetPagination
    authentication_classes = [FirebaseAuthentication]
    permission_classes = [IsAuthenticated]

    def put(self, request, player_id):
        try:
            player = Player.objects.get(id=player_id)
            player_serializer = PlayerUpdateSerializer(player, data=request.data)
            logger.error(player_serializer)
            if player_serializer.is_valid():
                player_serializer.save()
                return Response(player_serializer.data)
            return Response(player_serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Player.DoesNotExist:
            raise Http404
    # def get_serializer_class(self):
         # # Define your HTTP method-to-serializer mapping freely.
         # # This also works with CoreAPI and Swagger documentation,
         # # which produces clean and readable API documentation,
         # # so I have chosen to believe this is the way the
         # # Django REST Framework author intended things to work:
         # if self.request.method in ['GET']:
             # # Since the Serializer does nested lookups
             # # in multiple tables, only use it when necessary
             # return PlayerSerializer
         # return PlayerUpdateSerializer


class TeamList(ListAPIView):
    pagination_class = PageNumberPaginationDataOnly
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_backends = [OrderingFilter]
    ordering_fields = ['name']


# Return the default tiersheet for unauthenticated users
class RankingsDefault(ListAPIView):
    def get(self, request):
        rankings = self.get_default_tiersheet()
        tiersheet = RankingDetailSerializer(rankings, many=True)
        return Response(tiersheet.data)

    def get_default_tiersheet(self):
        default = Tiersheet.objects.get(is_default=True)
        rankings = default.ranking_set.all().order_by("rank")
        return rankings


class TiersheetList(ListAPIView):
    serializer_class = TiersheetSerializer
    authentication_classes = [FirebaseAuthentication]

    def get_queryset(self):
        user = self.request.user
        return Tiersheet.objects.filter(user=user)

    # Create and save a new tiersheet
    def post(self, request):
        user = request.user
        new_tiersheet = Tiersheet.objects.create(user=user)
        rankings = self.get_default_tiersheet()
        for q in rankings:
            q.tiersheet_id = new_tiersheet.id
            q.id = None
            q.is_starred = False
            q.is_default = False
        Ranking.objects.bulk_create(rankings)
        return Response(new_tiersheet.id, status=HTTP_200_OK)

    def get_default_tiersheet(self):
        default = Tiersheet.objects.get(is_default=True)
        rankings = default.ranking_set.all()
        return rankings


class RankingsDetail(APIView):
    authentication_classes = [FirebaseAuthentication]
    permission_classes = [IsAuthenticated, IsOwner]

    def get_object(self, pk):
        try:
            tiersheet = (
                Ranking.objects.filter(tiersheet_id=pk)
                .all()
                .select_related("player")
                .order_by("rank")
            )
            return tiersheet
        except Tiersheet.DoesNotExist:
            raise Http404

    # Get a tiersheet by id
    def get(self, request, pk, format=None):
        item = self.get_object(pk)
        tiersheet = RankingDetailSerializer(item, many=True)
        return Response(tiersheet.data)

    # Save a tiersheet
    def post(self, request, pk):
        rankings = request.data
        for index, player_id in enumerate(rankings):
            ranking = Ranking.objects.get(player_id=player_id, tiersheet_id=pk)
            ranking.rank = index
            ranking.save()

        return Response(status=HTTP_200_OK)

    # Delete a tiersheet by id
    def delete(self, request, pk, format=None):
        tiersheet = Tiersheet.objects.filter(id=pk)[0]
        if not tiersheet.is_default:
            tiersheet.delete()
            return Response(status=HTTP_200_OK)
        return Response(status=HTTP_400_BAD_REQUEST)


class Star(APIView):
    authentication_classes = [FirebaseAuthentication]
    permission_classes = [IsAuthenticated, IsOwner]

    # Favorite a player on a tiersheet
    def put(self, request, pk, player_id):
        try:
            ranking = Ranking.objects.get(player_id=player_id, tiersheet_id=pk)
            ranking.is_starred = not ranking.is_starred
            ranking.save()
            return Response(status=HTTP_200_OK)
        except Ranking.DoesNotExist:
            raise Http404
