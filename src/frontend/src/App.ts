import Vue from 'vue';
import Component from 'vue-class-component';
@Component({
  template: `
    <v-app>
      <router-view></router-view>
    </v-app>
  `,
})
export default class App extends Vue {
  public name = 'App';
  public components = {};
}
