import 'vuetify/dist/vuetify.min.css';
import Vue from 'vue';
import App from './App';
import { FirebasePlugin } from './plugins/auth';
import router from '@/router';
import store from '@/store/index';
import vuetify from '@/plugins/vuetify';
import { sync } from 'vuex-router-sync';
import Vuetify from 'vuetify';
import VueCookies from 'vue-cookies';
import jwtDecode, { JwtPayload } from 'jwt-decode';
import { Constants } from '@/config/constants';

Vue.config.productionTip = false;
Vue.use(VueCookies);
Vue.use(FirebasePlugin);
Vue.use(Vuetify);
sync(store, router);

const app = new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(App),
  async beforeMount() {
    const token = Vue.$cookies.get(Constants.JWT_PUBLIC_KEY);
    if (token) {
      try {
        const json = jwtDecode<AdminJwtPayload>(token);
        if (assertAlive(json)) {
          const isAdmin = !!json?.admin;
          await this.$store.dispatch('auth/loggedIn', isAdmin);
        } else {
          await this.$store.dispatch('auth/signOut');
        }
      } catch (error: any) {
        throw new Error(error);
      }
    }
  },
}).$mount('#app');

export { app, router, store };

export interface AdminJwtPayload extends JwtPayload {
  admin?: boolean;
}

export function assertAlive(decoded: AdminJwtPayload) {
  const now = Date.now().valueOf() / 1000;
  if (typeof decoded.exp !== 'undefined' && decoded.exp < now) {
    throw new Error(`token expired: ${JSON.stringify(decoded)} Signing Out...`);
  }
  if (typeof decoded.nbf !== 'undefined' && decoded.nbf > now) {
    throw new Error(`token not yet valid: ${JSON.stringify(decoded)} Signing Out...`);
  }
  return true;
}
