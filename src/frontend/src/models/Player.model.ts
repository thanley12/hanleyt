export enum Position {
  QB = 'QB',
  RB = 'RB',
  WR = 'WR',
  TE = 'TE',
  K = 'K',
  DST = 'DST',
}

export interface Tiersheet {
  id: string;
  title: string;
  is_default: boolean;
  date_created: string;
}

export interface Team {
  id: number;
  name: string;
  abrev: string;
  bye: number;
}

export interface Player {
  id: string;
  name: string;
  team: Team;
  position: Position;
  url: string;
}

export interface PlayerEditDto {
  id: string;
  name: string;
  team_id: number;
  position: Position;
  url: string;
}

export interface Ranking {
  id: string;
  player: Player;
  rank: number;
  is_starred: boolean;
  tiersheet: number;
  isTier: boolean;
  isHighlighted: boolean;
}
