export interface SnackbarState {
  show: boolean;
  text: string;
  left: boolean;
  right: boolean;
  hasClose: boolean;
  timeout: number;
  customLink: string;
}

export interface SnackbarConfig {
  text: string;
  left?: boolean;
  right?: boolean;
  timeout?: number;
  hasClose?: boolean;
  customLink?: string;
  trackCode?: string;
}
