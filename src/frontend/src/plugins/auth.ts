import firebase from 'firebase/app';
import { firebaseConfig } from '@/config/firebase_config';
import _Vue from 'vue';

export function FirebasePlugin(Vue: typeof _Vue, options?: any) {
  const fb = firebase.initializeApp(firebaseConfig);
  const auth = firebase.auth();
  auth.setPersistence(firebase.auth.Auth.Persistence.NONE);

  Vue.prototype.$firebase = firebase;
}
