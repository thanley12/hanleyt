import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/styles/main.sass';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import { theme } from './theme';

Vue.use(Vuetify);

export default new Vuetify(theme);
