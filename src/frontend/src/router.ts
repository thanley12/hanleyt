import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/layout/Home.vue';
import Players from '@/features/Players.vue';
import Login from '@/layout/Login.vue';
import Signup from '@/layout/Signup.vue';
import NotFound from '@/layout/NotFound.vue';
import NotAllowed from '@/layout/NotAllowed.vue';
import CreateTiersheet from '@/features/CreateTiersheet.vue';
import ResetPass from '@/layout/ResetPass.vue';
import ResetPassDone from '@/layout/ResetPassDone.vue';
import { Constants } from '@/config/constants';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/tiersheet/',
      name: 'create',
      component: CreateTiersheet,
    },
    {
      path: '/tiersheet/:id',
      name: 'players',
      component: Players,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/reset-password',
      name: 'reset-password',
      component: ResetPass,
    },
    {
      path: '/reset-password/done',
      name: 'reset-password-done',
      component: ResetPassDone,
    },
    {
      path: '/not-allowed',
      name: 'not-allowed',
      component: NotAllowed,
    },
    {
      path: '*',
      name: 'notfound',
      component: NotFound,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.authRequired)) {
    const sessionCookie = Vue.$cookies.get(Constants.JWT_PUBLIC_KEY);
    if (!sessionCookie) {
      // user doesn't have access token, redirect to login
      next({ name: 'login' });
    } else {
      next();
    }
  } else {
    // user has access token, user can open the page
    next();
  }
});

export default router;
