import httpClient from './httpClient';
import firebase from 'firebase/app';
import 'firebase/auth';
import { Player, PlayerEditDto } from '@/models/Player.model';

/* Players */

const PLAYER_ENDPOINT = '/players';
const TIERSHEET_ENDPOINT = '/tiersheets';
const RANKINGS_ENDPOINT = '/rankings';
const TEAMS_ENDPOINT = '/teams?ordering=name';

const updatePlayer = (player: PlayerEditDto) =>
  httpClient.put(`${PLAYER_ENDPOINT}/${player.id}`, player);
const getTeams = () => httpClient.get(TEAMS_ENDPOINT);
const getTiersheet = (id: string) => httpClient.get(`${RANKINGS_ENDPOINT}/${id}`);
const getTiersheets = () => httpClient.get(`${TIERSHEET_ENDPOINT}/`);
const getDefaultTiersheet = () => httpClient.get(`${RANKINGS_ENDPOINT}/`);
const createTiersheet = () => httpClient.post(`${TIERSHEET_ENDPOINT}/`);
const deleteTiersheet = (id: string) => httpClient.delete(`${RANKINGS_ENDPOINT}/${id}/`);
const toggleStar = (id: string, player: Player) =>
  httpClient.put(`${RANKINGS_ENDPOINT}/${id}/star/${player.id}`, {});
const reorder = (id: string, playerIds: string[]) =>
  httpClient.post(`${RANKINGS_ENDPOINT}/${id}/`, playerIds);

const postIdTokenToSessionLogin = (idToken: string) =>
  httpClient.post('/authenticate', { idToken });

/* Firebase */

const signUp = async (email: string, password: string) =>
  firebase.auth().createUserWithEmailAndPassword(email, password);

const login = async (email: string, password: string) =>
  firebase.auth().signInWithEmailAndPassword(email, password);

const resetPass = async (email: string) => firebase.auth().sendPasswordResetEmail(email);

const signOut = async () => firebase.auth().signOut();

export {
  updatePlayer,
  login,
  signUp,
  signOut,
  toggleStar,
  reorder,
  postIdTokenToSessionLogin,
  getTiersheet,
  getTiersheets,
  getDefaultTiersheet,
  createTiersheet,
  deleteTiersheet,
  getTeams,
  resetPass,
};
