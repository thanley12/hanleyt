declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

import Vue from 'vue';
import { VueCookies } from 'vue-cookies';

declare module 'vue/types/vue' {
  interface Vue {
    $firebase: any;
    $cookies: VueCookies;
  }
}
