import Vue from 'vue';
import router from '../router';
import firebase from 'firebase/app';
import jwtDecode from 'jwt-decode';
import { AdminJwtPayload } from '../main';
import { Constants } from '../config/constants';
import { SnackbarConfig } from '@/models/Snackbar.model';
import { login, postIdTokenToSessionLogin, signOut, signUp, resetPass } from '../services/services';

export const auth = {
  namespaced: true,

  state: {
    isLoggedIn: false,
    isAdmin: false,
    user: null,
  },

  getters: {
    user(state: any) {
      return state.user;
    },

    isLoggedIn(state: any) {
      return !!state.isLoggedIn;
    },

    isAdmin(state: any) {
      return state.isAdmin;
    },
  },

  mutations: {
    LOGIN_SUCCESS(state: any, isAdmin: boolean = false) {
      state.isAdmin = isAdmin;
      state.isLoggedIn = true;
    },

    RESET_USER(state: any) {
      state.isLoggedIn = false;
      state.isAdmin = false;
    },
  },

  actions: {
    async signUp({ dispatch }: any, payload: { email: string; password: string }) {
      try {
        const { email, password } = { ...payload };
        const u: firebase.auth.UserCredential = await signUp(email, password);
        if (u?.user) {
          return u?.user.sendEmailVerification().then(() =>
            dispatch('snackbar/setSnackMsg', { text: 'Email Sent' } as SnackbarConfig, {
              root: true,
            })
          );
        }
      } catch (e) {
        throw e;
      }
    },
    async login({ commit, dispatch }: any, payload: { email: string; password: string }) {
      try {
        const { email, password } = { ...payload };
        login(email, password)
          .then((u: firebase.auth.UserCredential) => {
            if (u?.user) {
              return u.user.getIdToken().then((idToken: string) => {
                return postIdTokenToSessionLogin(idToken).then(() => {
                  try {
                    const token = Vue.$cookies.get(Constants.JWT_PUBLIC_KEY);
                    const json = jwtDecode<AdminJwtPayload>(token);
                    const isAdmin = !!json?.admin;
                    commit('LOGIN_SUCCESS', isAdmin);
                    return router.push({ path: '/' });
                  } catch (e) {
                    throw new Error(`Login Error: ${JSON.stringify(e)}`);
                  }
                });
              });
            }
          })
          .catch((error) => {
            const errorCode = error?.code;
            if (errorCode === 'auth/wrong-password') {
              dispatch(
                'snackbar/setSnackMsg',
                { text: 'You have entered an incorrect password.' } as SnackbarConfig,
                {
                  root: true,
                }
              );
            } else if (errorCode === 'auth/user-not-found') {
              dispatch(
                'snackbar/setSnackMsg',
                { text: 'This user is not found' } as SnackbarConfig,
                {
                  root: true,
                }
              );
            }
          });
      } catch (e) {
        throw e;
      }
    },
    async signOut({ commit }: any) {
      await signOut().then(() => {
        Vue.$cookies.remove(Constants.JWT_PUBLIC_KEY, '/', Constants.TIERSHEET_DOMAIN);
        commit('RESET_USER');
      });
    },
    async resetPassword({ commit }: any, email: string) {
      await resetPass(email).then(() => {
        return router.push({ path: '/reset-password/done' });
      });
    },

    loggedIn({ commit }: any, isAdmin: boolean) {
      commit('LOGIN_SUCCESS', isAdmin);
    },
  },
};
