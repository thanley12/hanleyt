import Vuex from 'vuex';
import Vue from 'vue';
import { auth } from './auth';
import { snackbar } from './snackbar';
import { players } from './players';
import { tiersheets } from './tiersheets';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    snackbar,
    players,
    tiersheets,
  },
  strict: process.env.NODE_ENV !== 'production',
});

export default store;
