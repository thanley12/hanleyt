import * as services from '@/services/services';
import { Player, PlayerEditDto } from '@/models/Player.model';
import { SnackbarConfig } from '@/models/Snackbar.model';

export const players = {
  namespaced: true,

  state: {
    teams: [],
    isLoading: false,
  },

  getters: {
    teams(state: any) {
      return state.teams;
    },
  },

  mutations: {
    GET_TEAMS_SUCCESS(state: any, teams: any) {
      state.teams = teams;
    },
    UPDATE_PLAYER_SUCCESS(state: any, player: any) {
      state.player = player;
    },
  },

  actions: {
    async getTeams({ commit }: any) {
      await services.getTeams().then((response) => {
        commit('GET_TEAMS_SUCCESS', response.data);
      });
    },
    async updatePlayer({ commit }: any, player: Player) {
      const mapped = mapToEdit(player);
      await services.updatePlayer(mapped).then((p) => {
        commit('UPDATE_PLAYER_SUCCESS', p);
        commit('snackbar/SET_SNACK', { text: 'Updated Player' } as SnackbarConfig, { root: true });
      });
    },
  },
};

export function mapToEdit(player: Player): PlayerEditDto {
  return {
    id: player.id,
    name: player.name,
    team_id: player.team.id,
    url: player.url,
    position: player.position,
  } as PlayerEditDto;
}
