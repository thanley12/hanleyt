import Vue from 'vue';
import { SnackbarConfig, SnackbarState } from '@/models/Snackbar.model';

const defaultState: SnackbarState = {
  show: false,
  text: '',
  left: false,
  right: false,
  timeout: 1500,
  hasClose: false,
  customLink: '',
};

export const snackbar = {
  namespaced: true,

  state: { snackConfig: defaultState },

  getters: {
    snackConfig(state: any) {
      return state.snackConfig;
    },
  },

  mutations: {
    SET_SNACK(state: any, snackConfig: SnackbarConfig) {
      state.snackConfig = Object.assign({}, state.snackConfig, snackConfig, { show: true });
    },
    CLEAR(state: any) {
      state.snackConfig = { ...defaultState };
    },
  },

  actions: {
    setSnackMsg({ commit }: any, config: SnackbarConfig) {
      commit('SET_SNACK', config);
    },
    clear({ commit }: any) {
      commit('CLEAR');
    },
  },
};
