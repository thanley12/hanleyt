import * as services from '@/services/services';
import { Ranking } from '@/models/Player.model';
import { SnackbarConfig } from '@/models/Snackbar.model';

export const tiersheets = {
  namespaced: true,

  state: {
    tiersheets: [],
    error: '',
    isLoading: false,
  },

  getters: {
    tiersheets(state: any) {
      return state.tiersheets;
    },
    isLoading(state: any) {
      return state.isLoading;
    },
    // qbs(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.QB]);
    // },
    // rbs(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.RB]);
    // },
    // wrs(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.WR]);
    // },
    // tes(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.TE]);
    // },
    // dsts(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.DST]);
    // },
    // ks(state: any) {
    // return state.tiersheets.filter((x: Ranking) => x.player.position === Position[Position.K]);
    // }
  },

  mutations: {
    CLEAR_TIERSHEETS(state: any) {
      state.tiersheets = [];
    },
    GET_TIERSHEETS_SUCCESS(state: any, payload: any) {
      state.tiersheets = payload;
    },

    DELETE_TIERSHEET_SUCCESS(state: any, id: string) {
      state.tiersheets = state.tiersheets.filter((x: any) => x.id !== id);
    },
    TOGGLE_LOADING(state: any) {
      state.isLoading = !state.isLoading;
    },
    TIERSHEET_FAIL(state: any, error: string) {
      state.error = error;
    },
  },

  actions: {
    clear({ commit }: any) {
      commit('CLEAR_TIERSHEETS');
    },
    async getTiersheet({ commit }: any, id: string) {
      commit('TOGGLE_LOADING');
      await services.getTiersheet(id).then((response) => {
        response.data.forEach((x: any) => (x.isHighlighted = false));
        response.data.map((x: any) => (isTier(x) ? (x.isTier = true) : (x.isTier = false)));
        commit('GET_TIERSHEET_SUCCESS', response.data);
        commit('TOGGLE_LOADING');
      });
    },
    async getTiersheets({ commit }: any) {
      commit('TOGGLE_LOADING');
      await services.getTiersheets().then(
        (response) => {
          commit('GET_TIERSHEETS_SUCCESS', response.data.results);
          commit('TOGGLE_LOADING');
        },
        (err) => {
          commit('TIERSHEET_FAIL', err);
        }
      );
    },
    async deleteTiersheet({ commit, dispatch }: any, id: string) {
      await services.deleteTiersheet(id).then(() => {
        commit('DELETE_TIERSHEET_SUCCESS', id);
        dispatch('snackbar/setSnackMsg', { text: 'Tiersheet Deleted' } as SnackbarConfig, {
          root: true,
        });
      });
    },
  },
};

export function isTier(ranking: Ranking): boolean {
  return ranking.player.name.toLowerCase().indexOf('tier') >= 0;
}
