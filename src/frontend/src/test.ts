// import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
// import axios from 'axios';
// import BuddyGeneratorApp from 'buddy_generator_app.vue';
// import { teamYml, teamJson } from './constants';

// jest.mock('axios');
// const localVue = createLocalVue();
// describe('Buddy Generator App', () => {
// test('converts teamYml into json', done => {
// const component = mount(BuddyGeneratorApp);

// axios.get.mockResolvedValue({
// data: teamYml,
// });

// component.vm.getTeamData().then(teamData => {
// expect(teamData[0].name).toEqual(teamJson[0].name);
// expect(teamData[1].name).toEqual(teamJson[1].name);
// expect(teamData[2].name).toEqual(teamJson[2].name);
// expect(teamData[3].name).toEqual(teamJson[3].name);
// done();
// });
// });

// test('loads data', async () => {
// const methods = {
// getTeamData: jest.fn(),
// getDepartments: jest.fn(),
// };
// const component = shallowMount(BuddyGeneratorApp, {
// localVue,
// methods,
// });

// component.vm.teamData = teamJson;
// expect(methods.getTeamData).toHaveBeenCalled();
// });

// test('selects a random person', () => {
// const component = mount(BuddyGeneratorApp);
// component.vm.teamData = teamJson;
// component.vm.selectedDept = component.vm.teamData[0].departments[0];
// const randomPerson = component.vm.selectRandomPerson(component.vm.selectedDept);
// expect(randomPerson.departments[0]).not.toEqual(component.vm.selectedDept);
// expect(randomPerson).toBeTruthy();
// });

// test('filters teamData down to departments', async () => {
// const component = mount(BuddyGeneratorApp);
// component.vm.teamData = teamJson;

// const departments = await component.vm.getDepartments(component.vm.teamData);
// expect(departments.length >= 0).toBeTruthy();
// });

// test('generates teamData and selected person', async () => {
// const component = mount(BuddyGeneratorApp);
// component.vm.getTeamData = jest.fn(() => Promise.resolve(teamJson));

// component.vm.generate().then(() => {
// expect(component.vm.teamData[0].name).toEqual(teamJson[0].name);
// expect(component.vm.teamData[1].name).toEqual(teamJson[1].name);
// expect(component.vm.teamData[2].name).toEqual(teamJson[2].name);
// expect(component.vm.teamData[3].name).toEqual(teamJson[3].name);
// expect(component.vm.selected >= 0).toBeTruthy();
// expect(component.vm.selected).toBeLessThan(teamJson.length);

// component.vm.$nextTick(() => {
// expect(component.vm.$el.querySelector('h1').innerHTML).toEqual(
// 'Your randomly selected onboarding buddy is',
// );
// expect(component.vm.$el.querySelector('p').innerHTML.length).toBeGreaterThan(0);
// done();
// });
// });
// });

// test('calls generate when button clicked', () => {
// const component = mount(BuddyGeneratorApp);

// component.setMethods({
// generate: jest.fn(),
// });

// component.find('button').trigger('click');

// expect(component.vm.generate).toHaveBeenCalled();
// });
// });
