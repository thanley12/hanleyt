#
server {
    listen 80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name 127.0.0.1;
    ssl_certificate /run/secrets/fullchain.pem;
    ssl_certificate_key /run/secrets/privkey.pem;
    ssl_protocols TLSv1.2 TLSv1.3;
    charset utf-8;

    location /static {
        autoindex on;
        alias /static/;
    }

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;

        proxy_pass http://frontend:8080/;

        try_files $uri $uri/ /index.html;
    }

    location /api/ {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # enable this if and only if you use HTTPS
        proxy_set_header X-Forwarded--Proto https;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_pass http://backend:8000/;
    }


    location ~ /.well-known/acme-challenge {
        allow all;
        root /usr/share/nginx/html;
    }
}
