
server {
    listen 80;
    listen [::]:80;
    server_name fantasytiersheet.com www.fantasytiersheet.com;

    location /.well-known/acme-challenge/ {
       	default_type "text/plain";
        root  /data/letsencrypt/;
    }

    location / {
        return 301 https://$server_name$request_uri;
    }
}

server {
    listen 443                ssl http2;
    listen [::]:443           ssl http2;
    server_name               fantasytiersheet.com www.fantasytiersheet.com;

    ssl                       on;

    add_header                Strict-Transport-Security "max-age=31536000" always;
    ssl_session_timeout       1d;
    ssl_session_tickets       off;

    ssl_certificate           /run/secrets/fullchain.pem;
    ssl_certificate_key       /run/secrets/privkey.pem;
    ssl_trusted_certificate   /run/secrets/chain.pem;

    ssl_protocols TLSv1.2 TLSv1.3;

    ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';

    ssl_prefer_server_ciphers on;
    ssl_stapling              on;
    ssl_stapling_verify       on;
    resolver                  8.8.8.8 8.8.4.4;

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    charset utf-8;
    root /usr/share/nginx/html/;

    location / {
        #security headers
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-XSS-Protection "1; mode=block" always;
        add_header X-Content-Type-Options "nosniff" always;
        add_header X-Frame-Options "DENY" always;
        #CSP
        add_header Content-Security-Policy "frame-src 'self'; default-src 'self' *.fantasytiersheet.com *.googleapis.com; script-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src 'self'; style-src 'self' 'unsafe-inline' *.fantasytiersheet.com *.googleapis.com; font-src 'self' data: *.googleapis.com *.bootstrapcdn.com *.gstatic.com; form-action 'self'; upgrade-insecure-requests;" always;
        add_header Referrer-Policy "strict-origin-when-cross-origin" always;

        try_files $uri $uri/ /index.html;
    }

    location /static {
         autoindex on;
         alias /static/;
    }

    location /api {
        proxy_pass http://backend:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /robots.txt {
        allow all;
        access_log off;
        log_not_found off;
    }

}



